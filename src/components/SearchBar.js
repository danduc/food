import React from 'react';
import { View, TextInput, StyleSheet} from 'react-native';
import { Feather } from '@expo/vector-icons';

const SearchBar = ( { term, onTermChange, onTermSubmit }) => {
    return <View style={ styles.backgroundStyle }>
        <Feather name='search' style={ styles.iconStyle } />
        <TextInput style={ styles.inputStyle } 
                   autoCorrect={ false }
                   autoCapitalize="none"
                   value={ term } 
                   placeholder="Search"
                   onChangeText={ onTermChange } 
                   onEndEditing={ onTermSubmit } />
    </View>
};

const styles = StyleSheet.create({
    backgroundStyle : {
        backgroundColor:'#f0eeee',
        marginTop:10,
        height:50,
        borderRadius:5,
        marginHorizontal:15,
        flexDirection:'row',
        marginBottom:10
    },
    inputStyle : {
        flex:1
    },
    iconStyle : {
        fontSize:35,
        alignSelf:'center',
        marginHorizontal:15
    }
});

export default SearchBar;


